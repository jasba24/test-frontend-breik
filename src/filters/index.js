const getContract = (contract) => {
  const numberContract = Number(contract);
  const numberContracts = {
    1: '40',
    2: '25',
  };
  return numberContracts[numberContract];
};

const getUserLocation = (location) => {
  const locations = {
    1: 'Ñuñoa',
    2: 'Providencia',
    3: 'Santiago',
  };
  return locations[location];
};

const getPosition = (position) => {
  const positions = {
    1: 'Copero/a',
    2: 'Recepcionista',
    3: 'Chef',
    4: 'Garzón/a',
    6: 'Barista',
    7: 'Supervisor/a',
    8: 'Aseo',
  };
  return positions[position];
};

const getTemplateLocation = (location) => {
  if (location.length === 0) return 'undefined';
  if (location.length === 1) {
    const locationItem = location[0];
    const locations = {
      1: 'Ñuñoa',
      2: 'Providencia',
      3: 'Santiago',
    };
    return locations[locationItem];
  }
  if (location.length === 2) {
    const locationlength = location;

    if (locationlength[0] === 1 && location[1] === 2)
      return 'Ñuñoa, Providencia';
    if (locationlength[0] === 2 && location[1] === 3)
      return 'Providencia, Santiago';
  }
  if (location.length === 3) {
    const locationlength = location;

    if (
      locationlength[0] === 1 &&
      locationlength[1] === 2 &&
      locationlength[2] === 3
    )
      return 'Ñuñoa, Providencia, Santiago';
  }
};

const getTurnByUser = (user) => {
  const turns = {
    Cristian: 'Intermedio Chef \n (15:00 - 22:00)',
    Jonathan: 'Intermedio Aseo \n (13:00 - 21:00)',
    Nicole: 'Cierre Barista \n  (17:00 - 23:00)',
    Karen: 'Cierre Copero/a \n (09:00 - 18:00)',
    Cynthia: 'Intermedio Barista \n (13:00 - 20:00)',
    Jennifer: 'Apertura \n Supervisor \n (07:00 - 13:00)',
    Ariel: 'Intermedio Chef \n (15:00 - 22:00)',
    Beatriz: 'Cierre Garzón/a \n (16:00 - 23:00)',
    Marina: 'Apertura Barista \n (07:00 - 14:00)',
    Vaneza: 'Cierre Copero/a \n (09:00 - 18:00)',
    Jhonathan: 'Apertura Aseo \n (07:00 - 14:00)',
    Karla: 'Apertura Aseo \n (07:00 - 14:00)',
    Patricio: 'Apertura Aseo \n (07:00 - 14:00)',
  };

  return turns[user];
};

export {
  getContract,
  getUserLocation,
  getPosition,
  getTemplateLocation,
  getTurnByUser,
};
