import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userList: null,
    id: null,
    index: null,
    data: {
      firstName: null,
      lastName: null,
      email: null,
      id: null,
      dateOfBirth: null,
      contract: null,
      employeeId: null,
    },
    userNameToDelete: null,
  },
  mutations: {
    setUser(state, data) {
      state.userList = data;
    },
    addUser(state, data) {
      state.userList.push(data);
    },
    deleteUser(state, { userId, confirm }) {
      if (!confirm) {
        state.id = userId;
        const userData = state.userList.filter((user) => user.id === userId);
        const id = state.userList.map((user) => user.id === userData[0].id);
        this.state.index = id.indexOf(true);
      }

      state.userNameToDelete = state.userList[this.state.index].firstName;
      if (confirm) {
        if (this.state.index === -1) {
          this.state.userList.splice(this.state.index, 1);
        }
        this.state.userList.splice(this.state.index, 1);
      }
    },
    updateUser(state, { userProp, confirm }) {
      if (!confirm) {
        const user = state.userList.map((user) => user === userProp);
        const id = user.indexOf(true);
        this.state.id = id;
      }

      state.data.id = userProp.id;
      state.data.firstName = userProp.firstName;
      state.data.lastName = userProp.lastName;
      state.data.email = userProp.email;
      state.data.dateOfBirth = userProp.dateOfBirth;
      state.data.contract = userProp.contract;
      state.data.employeeId = userProp.employeeId;

      if (confirm) {
        state.userList[this.state.id].id = userProp.id;
        state.userList[this.state.id].firstName = userProp.firstName;
        state.userList[this.state.id].lastName = userProp.lastName;
        state.userList[this.state.id].email = userProp.email;
        state.userList[this.state.id].dateOfBirth = userProp.dateOfBirth;
        state.userList[this.state.id].contract = userProp.contract;
        state.userList[this.state.id].employeeId = userProp.employeeId;
      }
    },
  },
});
