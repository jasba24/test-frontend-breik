import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import {
  getContract,
  getUserLocation,
  getPosition,
  getTemplateLocation,
  getTurnByUser,
} from './filters';
import './assets/tailwind.css';

Vue.config.productionTip = false;

Vue.filter('contract', getContract);
Vue.filter('location', getUserLocation);
Vue.filter('position', getPosition);
Vue.filter('template', getTemplateLocation);
Vue.filter('user', getTurnByUser);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
