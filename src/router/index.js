import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Calendar from '@/views/Calendar.vue';
import Users from '@/views/Users.vue';
import Locations from '@/views/Locations';
import TurnTemplates from '@/views/TurnTemplates';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: Calendar,
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
  },
  {
    path: '/locations',
    name: 'Locations',
    component: Locations,
  },
  {
    path: '/turnTemplates',
    name: 'TurnTemplates',
    component: TurnTemplates,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
